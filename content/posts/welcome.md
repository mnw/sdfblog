---
title: "Welcome to the New mnw *log"
date: 2022-01-23
draft: false
---

--------------------

      Welcome!     

--------------------

Hello and thank you for visiting my online log. I've been making an earnest effort to record daily activity and write up events that are going on in my day to day. I've been able to keep up with it for a couple weeks, so I am going to try it out online.  I'll try to use tags and folders to keep things organized. 

I'm going to be using hugo as the static site generator, and I am using this incredible "theme" produced by Dr Mike Marin called [Hugo-2-Gopher-and-Gemini](https://github.com/mkamarin/Hugo-2-Gopher-and-Gemini) If you see anything funky on http/gopher/gemini let me know via email or in COM during DGC :-)  

All the content on this site is licensed under a creative commons by attribution share alike license.


